BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "subscribers" (
	"username"	TEXT NOT NULL,
	"email"	        TEXT NOT NULL UNIQUE,
	"welcomed"	INTEGER NOT NULL DEFAULT 0 CHECK("welcomed" IN (0, 1))
);
COMMIT;
