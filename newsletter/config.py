from pathlib import Path

goodbye_subject = "We are sad to see you go 🙁"
welcome_subject = "🙏 Welcome to Siya Web Services Newsletter"
goodbye_email = Path("../goodbye_email.html").read_text()
welcome_email = Path("../welcome_email.html").read_text()
