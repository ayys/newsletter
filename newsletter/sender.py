'''
Newsletter class qhich has the following methods
'''
from email.message import EmailMessage
from smtplib import SMTP
import sqlite3
from pathlib import Path

import config


class Email(EmailMessage):
    def __init__(self, subject, toMail, fromMail, body):
        super().__init__()
        self['Subject'] = subject
        self['From'] = fromMail
        self['To'] = toMail
        self.set_content(body, subtype='html')


class Newsletter:
    def __init__(self, database: str, fromMail, tls=True):
        self.connection = sqlite3.connect(database)
        self.smtp = SMTP("localhost")
        if tls:
            self.smtp.starttls()
        self.fromMail = fromMail

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.connection.close()
        self.smtp.close()

    def __send(self, toMail, subject, body):
        self.smtp.send_message(Email(subject, toMail, self.fromMail, body))

    def __getAllEmailAddresses(self):
        rows = self.connection.execute("select email from subscribers")
        return [col[0] for col in rows]

    def __sendGoodbyeEmail(self, name, address):
        self.__send(address, config.goodbye_subject, config.goodbye_email)

    def __sendWelcomeEmail(self, name, address):
        self.__send(address, config.welcome_subject, config.welcome_email)

    def __addEmailToDatabase(self, name, email):
        self.connection.execute(
            '''INSERT into subscribers (username, email) values ("{}", "{}")'''
            .format(name, email))
        self.connection.commit()

    def __removeEmailFromDatabase(self, email):
        self.connection.execute(
            '''delete from subscribers where email IS "{}"'''
            .format(email))
        self.connection.commit()

    def sendToAll(self, subject, body):
        for mail in self.__getAllEmailAddresses():
            self.__send(mail, subject, body)

    def subscribe(self, name: str, address: str):
        self.__sendWelcomeEmail(name, address)
        self.__addEmailToDatabase(name, address)

    def unsubscribe(self, name: str, address: str):
        self.__sendGoodbyeEmail(name, address)
        self.__removeEmailFromDatabase(address)
